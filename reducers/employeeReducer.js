// employeeReducer.js

import * as actionTypes from '../actions/actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case actionTypes.CREATE_NEW_EMPLOYEE:
      alert('added')
      return [
        ...state,
        Object.assign({}, action.employees)
      ];
    case actionTypes.REMOVE_EMPLOYEE:
      return state.filter((data, i) => i !== action.id);

    case actionTypes.UPDATE_EMPLOYEE:
      let abc = [...state];
      abc[action.index] = action.employees
      state = abc;
      return [...state]

    default:
      return state;

  }
};