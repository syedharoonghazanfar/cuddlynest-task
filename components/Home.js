import React, { Component } from 'react'
import { Text, View, Button, Image, StyleSheet, Modal } from 'react-native'
import EntryForm from './EntryForm'
import Employees from './Employees';

export default class Home extends Component {

    state = {
        formVisible: false,
        empVisible: false,
        empList: false,
    };
    handleAdd = (visible) => {
        this.setState({ formVisible: visible });
    }

    handleEmployees = (visible) => {
        this.setState({ empVisible: visible });
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../assets/icon.png')} />
                <Text style={styles.txt}>EMPLOYEE MANAGEMENT{'\n'}</Text>
                <Button color="#00ff00" title="Add Employees" onPress={() => { this.handleAdd(true); }} />
                <Button color="#00ff00" title="Previous Employees" onPress={() => { this.handleEmployees(true); }} />

                <Modal animationType="slide" transparent={false} visible={this.state.formVisible}>
                    <EntryForm />
                    <Button color="#00ff00" title="Back" onPress={() => { this.handleAdd(false); }} />
                </Modal>

                <Modal animationType="slide" transparent={false} visible={this.state.empVisible}>
                    <Employees />
                    <Button color="#00ff00" title="Back" onPress={() => { this.handleEmployees(false); }} />
                </Modal>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        backgroundColor: '#ffffff',
    },
    txt: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20,
    },
});