import React, { PureComponent } from 'react'
import { View, StyleSheet, Button, Image, Text, ScrollView, AppRegistry } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import t from 'tcomb-form-native'; // 0.6.9
import { connect } from 'react-redux';
import * as employeeAction from '../actions/employeeAction';

const Form = t.form.Form;

const User = t.struct({
  firstname: t.String,
  lastname: t.String,
  position: t.String,
  department: t.String,
  phonenumber: t.Number,
  isActive: t.Boolean
});


class EntryForm extends PureComponent {

  state = {
    form: {
      firstname: '',
      lastname: '',
      position: '',
      department: '',
      phonenumber: 0,
      isActive: false
    },
    reset: {
      firstname: '',
      lastname: '',
      position: '',
      department: '',
      phonenumber: 0,
      isActive: false
    }
  }

  handleSubmit = () => {
    let employee = {
      firstname: this.state.form.firstname,
      lastname: this.state.form.lastname,
      position: this.state.form.position,
      department: this.state.form.department,
      phonenumber: this.state.form.phonenumber,
      isActive: this.state.form.isActive
    }
    this.props.createEmployees(employee);
    this.setState(() => {
      return { form: this.state.reset }
    });
  }

  onChange = (e) => {
    this.setState(() => {
      return { form: e }
    });
  }

  render() {
    return (
      <>
        <ScrollView>
          <View style={styles.container}>
            <KeyboardAwareScrollView>
              <Image style={styles.img} source={require('../assets/icon.png')} />
              <Text style={styles.txt}>ADD {'\n'}</Text>
              <Form type={User} ref='form' value={this.state.form} onChange={this.onChange} />
              <Button color="#00ff00" title="Add" onPress={this.handleSubmit} />
            </KeyboardAwareScrollView>
          </View>
        </ScrollView>
      </>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: "center"
  },
  img: {

    alignSelf: "center"
  },
});


AppRegistry.registerComponent('form', () => EntryForm)

const mapDispatchToProps = (dispatch) => {
  return {
    createEmployees: employee => dispatch(employeeAction.createEmployee(employee)),

  }
};

export default connect(null, mapDispatchToProps)(EntryForm);