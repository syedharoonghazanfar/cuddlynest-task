import React, { Component } from 'react';
import { StyleSheet, View, AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import Home from './components/Home';
import configureStore from './store/configureStore';

export default class App extends Component {
  render() {
    const store = configureStore();
    return (
      <View style={styles.container}>
        <Provider store={store}>
          <Home />
        </Provider>

      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});
AppRegistry.registerComponent('app', () => App)