import React from 'react';
import Employees from '../components/Employees';

import renderer from 'react-test-renderer';

test('employees test', () => {
  const tree = renderer.create(<Employees />).toJSON();
  expect(tree).toMatchSnapshot();
});