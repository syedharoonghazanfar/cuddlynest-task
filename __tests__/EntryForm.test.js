import React from 'react';
import EntryForm from '../components/EntryForm';

import renderer from 'react-test-renderer';

test('entryform correctly', () => {
  const tree = renderer.create(<EntryForm />).toJSON();
  expect(tree).toMatchSnapshot();
});