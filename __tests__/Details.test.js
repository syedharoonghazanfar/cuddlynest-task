import React from 'react';
import Details from '../components/Details';

import renderer from 'react-test-renderer';

test('details test', () => {
  const tree = renderer.create(<Details />).toJSON();
  expect(tree).toMatchSnapshot();
});